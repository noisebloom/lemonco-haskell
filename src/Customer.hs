-- ysrivastava
module Customer where
import Items

genReaction :: Int -> IO()
genReaction 0 = return()
genReaction 1 = putStrLn "Customer: 'It's okay. Not the best lemonade I've ever had.'"
genReaction 2 = putStrLn "Customer: 'Not bad!'" 
genReaction 3 = putStrLn "Customer: 'Ahhh... That was refreshing.'"
genReaction 4 = putStrLn "Customer: 'Thanks. Hit the spot.'"
genReaction 5 = putStrLn "Customer: 'Wow! This stuff is amazing!'"
genReaction _ = return()

getRating :: Int -> Int
getRating 0 = -1
getRating 1 = 0
getRating 2 = 1
getRating 3 = 1
getRating 4 = 3
getRating 5 = 5
getRating _ = 0

getFunds :: Int -> Int 
getFunds 0 = 0
getFunds _ = Items.getLemonadeCost

getNewCustomer :: Int -> Bool 
getNewCustomer 0 = False
getNewCustomer _ = True

genCustomerOdds :: [(String,Item)] -> (Int,Int)
genCustomerOdds supply = getOddsRange (0,1) supply

getOddsRange :: (Int,Int) -> [(String,Item)] -> (Int,Int)
getOddsRange (0,0) _ = (0,0)
getOddsRange odds (s:xs)
    | item == "Lemons" && qty <= 0 = getOddsRange (0,0) xs
    | item == "Cups" && qty <= 0 = getOddsRange (0,0) xs
    | item == "Ice" && qty >= Items.recipeCount "Ice" = getOddsRange (addOdds (0,2) odds) xs
    | item == "Sugar" && qty >= Items.recipeCount "Sugar" = getOddsRange (addOdds (0,3) odds) xs
    | otherwise = getOddsRange odds xs
    where item = fst s
          qty = quantity $ snd s
getOddsRange odds _ = odds

addOdds :: (Int,Int) -> (Int,Int) -> (Int,Int)
addOdds (0,1) s = s
addOdds s (0,1) = s
addOdds (0,2) (0,3) = (0,5)
addOdds (0,3) (0,2) = (0,5)
