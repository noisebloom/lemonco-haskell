-- ysrivastava
import Control.Concurrent
import Customer
import Items
import Menu
import Text.Read
import System.IO (hSetBuffering, stdout, BufferMode(NoBuffering))
import System.Process
import System.Random

-- Constants
hoursPerDay :: Int
hoursPerDay = 8

startingPlayerFunds :: Int
startingPlayerFunds = 20

-- Utils
clearScreen :: IO()
clearScreen = callCommand "clear"

-- Menu loop
-- isQuitGame -> isStartDay -> popularity -> playerSupply -> playerFunds -> Output
doMenu :: Bool -> Bool -> Int -> [(String,Item)] -> Int -> Int -> IO()
doMenu True _ _ _ _ _ = return() -- Quit game
doMenu False True popularity playerSupply totalFunds itemCount = startDay popularity hoursPerDay playerSupply totalFunds itemCount  -- Start Day
doMenu False False popularity playerSupply totalFunds itemCount = do clearScreen
                                                                     Menu.displayWallet totalFunds
                                                                     Menu.displayPopularity popularity >> putStrLn ""
                                                                     Menu.displaySupply playerSupply >> putStrLn ""
                                                                     Menu.displayMenu

                                                                     putStr "$ "
                                                                     resp <- (readMaybe <$> getLine)      -- Functor: input is in IO context, apply normal function and reapply context
                                                                                                          -- getLine      readMaybe
                                                                                                          -- IO String -> (String -> Maybe a) -> IO (Maybe a)
                                                                     -- Return which action was selected
                                                                     let menuAction = Menu.getMenuAction resp

                                                                     -- TODO: This seem backwards. Check if the item supply can be updated before deducting funds...
                                                                     let remainingFunds = Menu.deductFunds totalFunds menuAction
                                                                     let (newTotalFunds,newPlayerSupply) = Menu.updateItemSupply remainingFunds totalFunds playerSupply menuAction
                                                                     let newTotal = Menu.updateTotalInventory itemCount menuAction

                                                                     let isStart = Menu.isStartDay menuAction
                                                                     let isQuitGame = Menu.isQuitGame menuAction
                                                                     doMenu isQuitGame isStart popularity newPlayerSupply newTotalFunds newTotal

-- Day loop
startDay :: Int -> Int -> [(String,Item)] -> Int -> Int -> IO()
startDay _ _ _ 0 0 = putStrLn "You have no money or inventory: Game over."
startDay popularity 0 playerSupply totalFunds itemCount = doMenu False False popularity playerSupply totalFunds itemCount
startDay popularity remainingHours playerSupply totalFunds itemCount = do clearScreen
                                                                          putStrLn $ "Hours remaining: " ++ show remainingHours
                                                                          Menu.displayWallet totalFunds
                                                                          Menu.displayPopularity popularity >> putStrLn ""
                                                                          Menu.displaySupply playerSupply >> putStrLn ""
           
                                                                          (numSales,newPopularity) <- sellLemonade True (-1) popularity playerSupply
                                                                          
                                                                          let newTotalFunds = sum [totalFunds, product [numSales,Items.getLemonadeCost]]
                                                                          let newPlayerSupply = Items.deductPlayerSupply numSales playerSupply
           
                                                                          putStrLn "\nPush any key to continue..."
                                                                          _ <- getLine
           
                                                                          startDay newPopularity (remainingHours - 1) newPlayerSupply newTotalFunds itemCount
           
-- Sale sequence
sellLemonade :: Bool -> Int -> Int -> [(String,Item)] -> IO (Int,Int)
sellLemonade False numSales popularity _ = return (numSales,popularity)
sellLemonade True numSales popularity supply = do g <- newStdGen
                                                  let val = fst $ randomR (Customer.genCustomerOdds supply) g
                                                  Customer.genReaction val
                                                  threadDelay 750000 --750 ms
                                                  sellLemonade (Customer.getNewCustomer val) (succ numSales) (Customer.getRating val + popularity) supply

-- MAIN
main = do
    hSetBuffering stdout NoBuffering 
    let playerSupply = [("Lemons",Items.getNewItem),("Ice",Items.getNewItem),("Cups",Items.getNewItem),("Sugar",Items.getNewItem)]

    doMenu False False 0 playerSupply startingPlayerFunds Items.getTotalItemCount
