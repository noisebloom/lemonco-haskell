-- ysrivastava
module Menu where
import Items

-- Item/money display
displaySupply :: [(String,Item)] -> IO()
displaySupply [] = return()
displaySupply (s:xs) = do putStrLn $ fst s ++ ": " ++ show(quantity $ snd s)
                          displaySupply xs
                          
displayWallet :: Int -> IO()
displayWallet x = putStrLn $ "Funds: " ++ show x

displayPopularity :: Int -> IO()
displayPopularity x = putStrLn $ "Popularity: " ++ show x

-- Calculations
deductFunds :: Int -> Int -> Int
deductFunds x 1 = x - (Items.getItemCost $ Items.getItem 1)
deductFunds x 2 = x - (Items.getItemCost $ Items.getItem 2)
deductFunds x 3 = x - (Items.getItemCost $ Items.getItem 3)
deductFunds x 4 = x - (Items.getItemCost $ Items.getItem 4)
deductFunds x _ = x

updateTotalInventory n resp = n + (Items.getItemBuyQuantity $ Items.getItem resp)

-- Menu
displayMenu :: IO()
displayMenu = do putStrLn $ "1. Buy " ++ show(Items.getItemBuyQuantity $ Items.getItem 1) ++ " " ++ Items.getItem 1 ++ " ($" ++ show(Items.getItemCost $ Items.getItem 1) ++ ")"
                 putStrLn $ "2. Buy " ++ show(Items.getItemBuyQuantity $ Items.getItem 2) ++ " " ++ Items.getItem 2 ++ " ($" ++ show(Items.getItemCost $ Items.getItem 2) ++ ")"
                 putStrLn $ "3. Buy " ++ show(Items.getItemBuyQuantity $ Items.getItem 3) ++ " " ++ Items.getItem 3 ++ " ($" ++ show(Items.getItemCost $ Items.getItem 3) ++ ")"
                 putStrLn $ "4. Buy " ++ show(Items.getItemBuyQuantity $ Items.getItem 4) ++ " " ++ Items.getItem 4 ++ " ($" ++ show(Items.getItemCost $ Items.getItem 4) ++ ")"
                 putStrLn $ "5. Start Day"
                 putStrLn $ "6. Quit"

getMenuAction :: Maybe Int -> Int
getMenuAction (Just x) = x
getMenuAction Nothing = 0

isStartDay :: Int -> Bool
isStartDay 5 = True
isStartDay _ = False

isQuitGame :: Int -> Bool
isQuitGame 6 = True
isQuitGame _ = False

doMenuActionSupply :: [(String,Item)] -> Int -> [(String,Item)]
doMenuActionSupply [] _ = []
doMenuActionSupply (x:xs) 1
    | fst x == Items.getItem 1 = Items.purchaseItem x (Items.getItemBuyQuantity $ Items.getItem 1):xs 
    | otherwise = x:doMenuActionSupply xs 1
doMenuActionSupply (x:xs) 2
    | fst x == Items.getItem 2 = Items.purchaseItem x (Items.getItemBuyQuantity $ Items.getItem 2):xs 
    | otherwise = x:doMenuActionSupply xs 2
doMenuActionSupply (x:xs) 3
    | fst x == Items.getItem 3 = Items.purchaseItem x (Items.getItemBuyQuantity $ Items.getItem 3):xs 
    | otherwise = x:doMenuActionSupply xs 3 
doMenuActionSupply (x:xs) 4
    | fst x == Items.getItem 4 = Items.purchaseItem x (Items.getItemBuyQuantity $ Items.getItem 4):xs 
    | otherwise = x:doMenuActionSupply xs 4 
doMenuActionSupply s _ = s

-- If the user doesn't have enough money, don't "buy" anything - return the original funds
updateItemSupply :: Int -> Int -> [(String,Item)] -> Int -> (Int,[(String,Item)])
updateItemSupply newFunds originalFunds supply option
    | newFunds < 0 = (originalFunds, supply)
    | otherwise = (newFunds, doMenuActionSupply supply option)
