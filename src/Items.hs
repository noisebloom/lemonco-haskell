-- ysrivastava
module Items where

-- Item definition
data Item = Item { quantity :: Int } deriving Show

getNewItem :: Item
getNewItem = Item { quantity = 0 }

getTotalItemCount :: Int
getTotalItemCount = 0

-- Constants
recipeCount :: String -> Int
recipeCount "Lemons" = 1
recipeCount "Ice" = 4
recipeCount "Cups" = 1
recipeCount "Sugar" = 5
recipeCount _ = 0

-- Item Map
getItem :: Int -> String
getItem 1 = "Lemons"
getItem 2 = "Ice"
getItem 3 = "Cups"
getItem 4 = "Sugar"
getItem _ = ""

getItemBuyQuantity :: String -> Int
getItemBuyQuantity "Lemons" = 1
getItemBuyQuantity "Ice" = 50
getItemBuyQuantity "Cups" = 10
getItemBuyQuantity "Sugar" = 30
getItemBuyQuantity x = 0

getItemCost :: String -> Int
getItemCost "Lemons" = 1
getItemCost "Ice" = 5
getItemCost "Cups" = 2
getItemCost "Sugar" = 4
getItemCost x = 0

getLemonadeCost :: Int
getLemonadeCost = 2

purchaseItem :: (String,Item) -> Int -> (String,Item)
purchaseItem x n = (fst x, Item {quantity = (quantity $ snd x) + n})

deductPlayerSupply :: Int -> [(String,Item)] -> [(String,Item)]
deductPlayerSupply 0 supply = supply
deductPlayerSupply sales (s:xs) = deductPlayerSupply (sales-1) (processPlayerSupply sales (s:xs) [])
deductPlayerSupply _ supply = supply

processPlayerSupply :: Int -> [(String,Item)] -> [(String,Item)] -> [(String,Item)]
processPlayerSupply sales (s:xs) newsupply
    | qty >= needed = processPlayerSupply sales xs $ newsupply ++ [(name, Item { quantity = (qty-needed) })]
    | qty < needed = processPlayerSupply sales xs $ newsupply ++ [(name, Item { quantity = 0 })]
    where name = fst s
          qty = quantity $ snd s
          needed = recipeCount $ fst s
processPlayerSupply _ _ newsupply = newsupply
