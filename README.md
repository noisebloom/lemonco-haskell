# Lemon Co.

Lemon Co. is a lemonade stand simulator that allows you to work your way up
from simple streetside vendor to the owner of a lemonade empire!

You will need to pay for ice, lemons, sugar, and a variety of other items to
attract customers and make a profit.

# Prerequisites

Operating System: GNU Linux or FreeBSD distributions
Compilers: ghc

# Building and running Lemon Co. 

Run the following command in the base directory to build Lemon Co.

$ ghc Main.hs -o lemonco

To run Lemon Co., run the following command in the base directory:

$ ./lemonco
